#coding=utf-8
import re
import HTMLParser

#Internal style sheets or JavaScript 
script_sheet = re.compile(r"<(script|style).*?>.*?(</\1>)",
                          re.IGNORECASE | re.DOTALL)

#HTML comments - can contain ">"
comment = re.compile(r"<!--(.*?)-->", re.DOTALL)

#HTML tags: <any-text>
tag = re.compile(r"<.*?>", re.DOTALL)

#Consecutive whitespace characters
nwhites = re.compile(r"[\s]+")

#<p>, <div>, <br> tags and associated closing tags
p_div = re.compile(r"</?(p|div|br).*?>", 
                   re.IGNORECASE | re.DOTALL)

#Consecutive whitespace, but no newlines
nspace = re.compile("[^\S\n]+", re.UNICODE)

#At least two consecutive newlines
n2ret = re.compile("\n\n+")

#A return followed by a space
retspace = re.compile("(\n )")

#For converting HTML entities to unicode
html_parser = HTMLParser.HTMLParser()

def to_text(html):
    """Remove all HTML tags, but produce a nicely formatted text."""
    if html is None:
        return u""
    text = unicode(html)
    text = script_sheet.sub("", text)
    text = comment.sub("", text)
    text = nwhites.sub(" ", text)
    text = p_div.sub("\n", text) #convert <p>, <div>, <br> to "\n"
    text = tag.sub("", text)     #remove all tags
    text = html_parser.unescape(text)
    #Get whitespace right
    text = nspace.sub(" ", text)
    text = retspace.sub("\n", text)
    text = n2ret.sub("\n\n", text)
    text = text.strip()
    return text
