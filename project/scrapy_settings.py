
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

os.environ['DJANGO_SETTINGS_MODULE'] = 'project.settings'

BOT_NAME = 'project'
USER_AGENT = 'Mozilla/5.0'
LOG_ENABLED = False

SPIDER_MODULES = ['project.spiders']
NEWSPIDER_MODULE = 'project.spiders'
