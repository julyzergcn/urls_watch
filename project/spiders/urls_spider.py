from __future__ import with_statement
try:
    from cStringIO import StringIO
except ImportError:
    import StringIO
import re

from scrapy.spider import BaseSpider
try:
    from scrapy.selector import Selector
except ImportError:
    from scrapy.selector import HtmlXPathSelector as Selector
from scrapy.http import Request
from scrapy.utils.misc import md5sum
from scrapy.contrib.loader.processor import TakeFirst, Compose, MapCompose

from django.db import transaction
from django.utils import timezone
from django.utils.encoding import force_bytes

import reversion
from html2text import to_text
from pages.models import Page


def strip(value):
    return value.strip()


def sub(value):
    if u'\u2013' in value:
        value = value.replace(u'\u2013', '-')
    value = re.sub('\n+', '\n', value)
    value = re.sub('[\t\s]+', ' ', value)
    return value


def join(value):
    return ' '.join(value)


def strip_sub_join(value):
    return Compose(join, sub, strip)(value)


def checksum(text):
    return md5sum(StringIO(force_bytes(text)))


class UrlsSpider(BaseSpider):
    name = "urls"
    allowed_domains = []

    start_urls = [
        'http://localhost:8000/'
    ]

    def parse(self, response):
        s = Selector(response)
        for url in s.select('//a[@class="to_check"]/@href').extract():
            yield Request(url, self.parse_url)

    def parse_url(self, response):
        print '--', response

        page_content = to_text(response.body_as_unicode())

        with transaction.atomic():
            with reversion.create_revision():
                try:
                    page = Page.objects.get(url=response.url)
                except Page.DoesNotExist:
                    page = Page()
                    page.url = response.url
                    page.save()

                update_fields = []
                if checksum(page.html) != checksum(page_content):
                    page.html = page_content
                    update_fields.append('html')
                    if page.last_checked:
                        page.is_changed = True
                        page.last_changed = timezone.now()
                        update_fields.extend(['is_changed', 'last_changed'])
                else:
                    page.is_changed = False
                    update_fields = ['is_changed']
                page.last_checked = timezone.now()
                update_fields.append('last_checked')
                page.save(update_fields=update_fields)
