import subprocess
from celery import task


@task
def scrape_urls():
    subprocess.call('scrapy crawl urls', shell=True)
