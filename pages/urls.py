from django.conf.urls import patterns, include, url

urlpatterns = patterns('pages.views',
    url(r'^$', 'index', name='page_index'),
    url(r'^page/(\d+)/diff/$', 'page_diff', name='page_diff'),
)
