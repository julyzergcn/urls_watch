import reversion

from django.db import models
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'


class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class Page(models.Model):
    label = models.CharField(max_length=255, unique=True)
    url = models.URLField(max_length=255, unique=True)
    created = models.DateTimeField(default=timezone.now)
    last_checked = models.DateTimeField(null=True, blank=True)
    last_changed = models.DateTimeField(null=True, blank=True)
    is_changed = models.BooleanField(default=False)

    tags = models.ManyToManyField(Tag, null=True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)

    html = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        super(Page, self).save(*args, **kwargs)
        for name in self.label.split(' '):
            tag, created = Tag.objects.get_or_create(name=name)
            self.tags.add(tag)

reversion.register(Page)
