import reversion
from reversion.helpers import generate_patch_html

from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from .models import Page


def index(request):
    context = {
        'pages': Page.objects.all(),
    }
    return render(request, 'page_index.html', context)


def page_diff(request, page_id):
    page = get_object_or_404(Page, pk=page_id)
    versions = reversion.get_for_object(page)
    if len(versions) > 1:
        page1 = versions[0]
        page2 = versions[1]
    else:
        page1 = page
        page2 = page
    diff = generate_patch_html(page2, page1, 'html', cleanup='semantic')
    return HttpResponse(diff)
