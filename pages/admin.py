import reversion

from django.contrib import admin
from django.shortcuts import resolve_url

from .models import Page, Category, Tag


def _tags(obj):
    return ', '.join([tag.name for tag in obj.tags.all()])

_tags.allow_tags = True


def diff(obj):
    if obj.is_changed:
        url = resolve_url('page_diff', obj.id)
        return '<a href="%s" target="_blank">diff</a>' % url
    return ''

diff.allow_tags = True


#~ class PageAdmin(admin.ModelAdmin):
class PageAdmin(reversion.VersionAdmin):
    list_display = ('label', 'is_changed', diff, 'category', 'last_changed', 'last_checked')
    list_filter = ('is_changed', 'category', 'tags')
    fields = ('label', 'url', 'category', 'is_changed', 'last_changed', 'last_checked', 'created')
    #~ fields += ('html', )
    readonly_fields = ('last_checked', 'is_changed', 'last_changed', 'created')


admin.site.register(Page, PageAdmin)

admin.site.register(Category)
admin.site.register(Tag)